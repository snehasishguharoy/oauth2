package com.sirius.xm.outh2demo.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sirius.xm.outh2demo.model.User;

@Repository
public interface UserDao extends CrudRepository<User, Long> {
	User findByUsername(String username);

}
