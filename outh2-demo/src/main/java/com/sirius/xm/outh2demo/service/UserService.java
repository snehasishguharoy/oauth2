package com.sirius.xm.outh2demo.service;

import java.util.List;

import com.sirius.xm.outh2demo.model.User;

public interface UserService {
	User save(User user);

	List<User> findAll();

	void delete(long id);
}
